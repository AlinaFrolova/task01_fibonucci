package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.epam.Menu.menu;
import static com.epam.Task01_Fibonacci.firstNumOfInterval;
import static com.epam.Task01_Fibonacci.lastNumOfInterval;

public class Operations {
    public static void printOddValues() {

        for (int count = firstNumOfInterval;
             count < lastNumOfInterval + 1; count++) {
            if (count % 2 != 0) {
                System.out.print(count + " ");
            }
        }
        System.out.println();
    }

    public static void printEvenValues() throws IOException {
        for (int count = lastNumOfInterval;
             count >= firstNumOfInterval; count--) {
            if (count % 2 == 0) {
                System.out.print(count + " ");
            }
        }
        System.out.println();
    }

    public static void countFibonucciNumbers() throws IOException {
        System.out.println("Please enter the size of set"
                + " of Fibonucci numbers: ");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        int setSize = Integer.parseInt(reader.readLine());
        int[] arrayOfFibonacciNum = new int[setSize];
        arrayOfFibonacciNum[0] = 0;
        arrayOfFibonacciNum[1] = 1;
        System.out.print(0 + " " + 1 + " ");
        for (int count = 2; count < setSize; count++) {
            arrayOfFibonacciNum[count] = arrayOfFibonacciNum[count - 1]
                    + arrayOfFibonacciNum[count - 2];
            System.out.print(arrayOfFibonacciNum[count] + " ");
        }
        System.out.println();
    }

    public static void printSumOddNumbers() throws IOException {
        int sumOdd = 0;
        for (int count = firstNumOfInterval;
             count < lastNumOfInterval + 1; count++) {
            if (count % 2 != 0) {
                sumOdd += count;
            }
        }
        System.out.print(sumOdd);
        System.out.println();
    }

    public static void printSumEvenNumbers() throws IOException {
        int sumEven = 0;
        for (int count = lastNumOfInterval;
             count >= firstNumOfInterval; count--) {
            if (count % 2 == 0) {
                sumEven += count;
            }
        }
        System.out.print(sumEven);
        System.out.println();
    }

    public static void printPerc() throws IOException {
        System.out.println("Please enter the size of set"
                + " of Fibonucci numbers: ");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        int setSize = Integer.parseInt(reader.readLine());
        if(setSize<=0){
            try {
                throw new IncorrectSizeException();
            } catch (IncorrectSizeException e) {
                System.out.println("You entered an incorrect size of set,"
                        + " please, try again : ");
                menu();
            }
        }
        double quantityOfOdd = 1;
        double quantityOfEven = 1;
        try {
            int[] arrayOfFibonacciNum = new int[setSize];
            arrayOfFibonacciNum[0] = 0;
            arrayOfFibonacciNum[1] = 1;
            for (int count = 2; count < setSize; count++) {
                arrayOfFibonacciNum[count] = arrayOfFibonacciNum[count - 1]
                        + arrayOfFibonacciNum[count - 2];
                if (arrayOfFibonacciNum[count] % 2 == 0) {
                    quantityOfEven++;
                } else {
                    quantityOfOdd++;
                }
            }
            System.out.println("The percentage of even numbers is: "
                    + (quantityOfEven) / setSize * 100);
            System.out.println("The percentage of odd numbers is: "
                    + (quantityOfOdd) / setSize * 100);
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.print("You entered an incorrect size for set of Fibonucci numbers, please try again");
            printPerc();
        }

    }
}
