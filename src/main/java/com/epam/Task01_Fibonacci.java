package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task01_Fibonacci {
    private static Menu menu = new Menu();
    public static int firstNumOfInterval = 0;
    public static int lastNumOfInterval = 0;

    public static void main(final String[] args) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter the first number of the interval:");
        firstNumOfInterval = Integer.parseInt(reader.readLine());
        System.out.println("Please enter the last number of the interval:");
        lastNumOfInterval = Integer.parseInt(reader.readLine());
        if ((firstNumOfInterval > lastNumOfInterval) ||
                (firstNumOfInterval == lastNumOfInterval)||(firstNumOfInterval<0)) {
            try {
                throw new IntervalException();
            } catch (IntervalException e) {
                System.out.println("The values are incorrect, please, try again:");
                menu.menu();
            }
        }
        System.out.println();
        menu.menu();
    }
}
