package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.epam.Operations.printEvenValues;
import static com.epam.Operations.printSumEvenNumbers;

public class Menu {
    private static Operations operations = new Operations();

    public static void makeChoice() throws IOException {
        System.out.println("Enter '1' to exit");
        System.out.println("Enter '2' to print odd values");
        System.out.println("Enter '3' to print even values");
        System.out.println("Enter '4' to print the sum of odd numbers");
        System.out.println("Enter '5' to print the sum of even numbers");
        System.out.println("Enter '6' to print the set of Fibonacci numbers");
        System.out.println("Enter '7' to print the percentage"
                + " of odd and even numbers");
    }

    public static void menu() throws IOException {
        System.out.println();
        makeChoice();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());
        switch (number) {
            case 1:
                System.exit(0);
            case 2:
                operations.printOddValues();
                menu();
            case 3:
                printEvenValues();
                menu();
            case 4:
                operations.printSumOddNumbers();
                menu();
            case 5:
                printSumEvenNumbers();
                menu();
            case 6:
                operations.countFibonucciNumbers();
                menu();
            case 7:
                operations.printPerc();
                menu();

            default:
                System.out.println("You`ve entered incorrect value,"
                        + " please, try again");
                menu();
        }
    }
}
