package com.epam;

public class IncorrectSizeException extends Exception{
    public IncorrectSizeException() {
    }

    public IncorrectSizeException(String message) {
        super(message);
    }

    public IncorrectSizeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectSizeException(Throwable cause) {
        super(cause);
    }

    public IncorrectSizeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
