package com.epam;

public class IntervalException extends Exception {
    public IntervalException() {
    }

    public IntervalException(String message) {
        super(message);
    }

    public IntervalException(String message, Throwable cause) {
        super(message, cause);
    }

    public IntervalException(Throwable cause) {
        super(cause);
    }

    public IntervalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
